package com.belen.torneo.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.belen.torneo.domain.Grupo;
import com.belen.torneo.repository.GrupoRepository;

@Service
@Transactional
public class GrupoService {
	
	private final Logger log = LoggerFactory.getLogger(GrupoService.class);
	
	@Autowired
    GrupoRepository grupoRepository;
    
    public List<Grupo> listar() {
        log.debug("Request to get all Grupos");
        return grupoRepository.findAll();
    }
    
    public Grupo save(Grupo grupo) {
        log.debug("Request to get all Grupos");
        return grupoRepository.save(grupo);
    }
    
    public Optional<Grupo> listarGrupo(Integer grupo) {
        log.debug("Request to get all Grupos");
        return grupoRepository.findById(grupo);
    }

}
