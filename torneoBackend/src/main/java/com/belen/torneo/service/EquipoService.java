package com.belen.torneo.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.belen.torneo.domain.Equipo;
import com.belen.torneo.repository.EquipoRepository;

@Service
@Transactional
public class EquipoService {
	
	private final Logger log = LoggerFactory.getLogger(EquipoService.class);
	
	@Autowired
    EquipoRepository equipoRepository;
	
	public List<Equipo> listar() {
        log.debug("Request to get all Campeonatos");
        return equipoRepository.findAll();
    }
    
    public Equipo save(Equipo equipo) {
        log.debug("Request to get all Campeonatos");
        return equipoRepository.save(equipo);
    }

}
