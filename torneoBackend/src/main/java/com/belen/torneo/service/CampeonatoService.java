package com.belen.torneo.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.belen.torneo.domain.Campeonato;
import com.belen.torneo.repository.CampeonatoRepository;

@Service
@Transactional
public class CampeonatoService {
	
	private final Logger log = LoggerFactory.getLogger(CampeonatoService.class);
	
	@Autowired
    CampeonatoRepository campeonatoRepository;
    
    public List<Campeonato> listar() {
        log.debug("Request to get all Campeonatos");
        return campeonatoRepository.findAll();
    }
    
    public Campeonato save(Campeonato campeonato) {
        log.debug("Request to get all Campeonatos");
        return campeonatoRepository.save(campeonato);
    }
    
    public Optional<Campeonato> listarCampeonato(Integer campeonato) {
        log.debug("Request to get all Campeonatos");
        return campeonatoRepository.findById(campeonato);
    }


}
