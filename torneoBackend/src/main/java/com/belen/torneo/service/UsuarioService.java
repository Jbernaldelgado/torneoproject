package com.belen.torneo.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.belen.torneo.domain.Usuario;
import com.belen.torneo.repository.UsuarioRepository;

@Service
@Transactional
public class UsuarioService {
	
	private final Logger log = LoggerFactory.getLogger(UsuarioService.class);
	
	@Autowired
    UsuarioRepository usuarioRepository;
    
    public List<Usuario> listar() {
        log.debug("Request to get all Usuarios");
        return usuarioRepository.findAll();
    }
    
    public Usuario save(Usuario usuario) {
        log.debug("Request to get all Usuarios");
        return usuarioRepository.save(usuario);
    }
    
    public Usuario update(Usuario usuario) {
        log.debug("Request to get all Usuarios");
        return usuarioRepository.save(usuario);
    }
    
    public void delete(int id) {
        log.debug("Request to get all Usuarios");
        usuarioRepository.deleteById(id);
    }
    
    public List<Usuario> listarDelegados(Integer delegado) {
        log.debug("Request to get all Usuarios");
        return usuarioRepository.findByIdRol(delegado);
    }
    
    public Optional<Usuario> listarUsuario(Integer usuario) {
        log.debug("Request to get all Usuarios");
        return usuarioRepository.findById(usuario);
    }
}
