package com.belen.torneo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.belen.torneo.domain.Equipo;

@Repository
public interface EquipoRepository extends JpaRepository<Equipo, Integer>{

}
