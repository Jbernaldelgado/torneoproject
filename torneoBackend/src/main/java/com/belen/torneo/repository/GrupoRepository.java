package com.belen.torneo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.belen.torneo.domain.Grupo;

@Repository
public interface GrupoRepository extends JpaRepository<Grupo, Integer>{

}
