package com.belen.torneo.controller;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.belen.torneo.domain.Usuario;
import com.belen.torneo.service.UsuarioService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class UsuarioController {
	
	private final Logger log = LoggerFactory.getLogger(UsuarioController.class);
	
	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping("/usuarios/listar")
	public List<Usuario> getUsuarios() {
		log.debug("Request to get all usuarios");
        return usuarioService.listar();
	}

	@PostMapping("/usuarios")
    public ResponseEntity<Usuario> createUsuario(@RequestBody Usuario usuario) {
		try {
			if (usuario.getId() != null) {
	            throw new MalformedURLException("A new usuario cannot already have an ID");
	        }
	        Usuario usuarioGuardar = usuarioService.save(usuario);
	        return new ResponseEntity<>(usuarioGuardar, HttpStatus.CREATED);
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
	
	@GetMapping(value= "/usuarios/listarDelegados", params="delegado")
	public List<Usuario> getDelegados(@RequestParam Integer delegado) {
		log.debug("Request to get all usuarios");
        return usuarioService.listarDelegados(delegado);
	}
	
	@GetMapping(value= "/usuarios/listarUsuario", params="usuario")
	public Optional<Usuario> getUsuario(@RequestParam Integer usuario) {
		log.debug("Request to get all usuarios");
        return usuarioService.listarUsuario(usuario);
	}
	
	@PutMapping("/usuarios")  
	private ResponseEntity<Usuario> update(@RequestBody Usuario usuario) {
		try {
	        Usuario usuarioEditar = usuarioService.update(usuario);
	        return new ResponseEntity<>(usuarioEditar, HttpStatus.CREATED);
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}  
	
	@DeleteMapping("/usuarios/{id}")  
	private void delete(@PathVariable("id") int id) {
		usuarioService.delete(id);
	}  

}
