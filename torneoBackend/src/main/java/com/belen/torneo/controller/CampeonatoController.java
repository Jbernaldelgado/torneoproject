package com.belen.torneo.controller;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.belen.torneo.domain.Campeonato;
import com.belen.torneo.service.CampeonatoService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class CampeonatoController {
	
	private final Logger log = LoggerFactory.getLogger(CampeonatoController.class);
	
	@Autowired
	private CampeonatoService campeonatoService;
	
	@GetMapping("/campeonatos/listar")
	public List<Campeonato> getCampeonatos() {
		log.debug("Request to get all Campeonatos");
        return campeonatoService.listar();
	}
	
	@PostMapping("/campeonatos")
    public ResponseEntity<Campeonato> createCampeonato(@RequestBody Campeonato campeonato) {
		try {
			if (campeonato.getId() != null) {
	            throw new MalformedURLException("A new campeonato cannot already have an ID");
	        }
	        Campeonato campeonatoGuardar = campeonatoService.save(campeonato);
	        return new ResponseEntity<>(campeonatoGuardar, HttpStatus.CREATED);
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
	
	@GetMapping(value="/campeonatos/listarCampeonato", params="campeonato")
	public Optional<Campeonato> getCampeonato(@RequestParam Integer campeonato) {
		log.debug("Request to get all Campeonatos");
        return campeonatoService.listarCampeonato(campeonato);
	}
	
}
