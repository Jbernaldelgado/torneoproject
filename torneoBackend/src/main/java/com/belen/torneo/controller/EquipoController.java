package com.belen.torneo.controller;

import java.net.MalformedURLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.belen.torneo.domain.Equipo;
import com.belen.torneo.service.EquipoService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class EquipoController {
	
	private final Logger log = LoggerFactory.getLogger(CampeonatoController.class);
	
	@Autowired
	private EquipoService equipoService;
	
	@GetMapping("/equipos/listar")
	public List<Equipo> getEquipos() {
		log.debug("Request to get all Equipos");
        return equipoService.listar();
	}
	
	@PostMapping("/equipos")
    public ResponseEntity<Equipo> createEquipo(@RequestBody Equipo equipo) {
		try {
			if (equipo.getId() != null) {
	            throw new MalformedURLException("A new equipo cannot already have an ID");
	        }
	        Equipo equipooGuardar = equipoService.save(equipo);
	        return new ResponseEntity<>(equipooGuardar, HttpStatus.CREATED);
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

}
