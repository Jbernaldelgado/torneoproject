
package com.belen.torneo.controller;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.belen.torneo.domain.Grupo;
import com.belen.torneo.service.GrupoService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class GrupoController {
	
	private final Logger log = LoggerFactory.getLogger(GrupoController.class);
	
	@Autowired
	private GrupoService grupoService;
	
	@GetMapping("/grupos/listar")
	public List<Grupo> getGrupos() {
		log.debug("Request to get all Grupos");
        return grupoService.listar();
	}
	
	@PostMapping("/grupos")
    public ResponseEntity<Grupo> createGrupo(@RequestBody Grupo grupo) {
		try {
			if (grupo.getId() != null) {
	            throw new MalformedURLException("A new grupo cannot already have an ID");
	        }
	        Grupo grupoGuardar = grupoService.save(grupo);
	        return new ResponseEntity<>(grupoGuardar, HttpStatus.CREATED);
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
	
	@GetMapping(value="/grupos/listarGrupo", params="grupo")
	public Optional<Grupo> getGrupo(@RequestParam Integer grupo) {
		log.debug("Request to get all Grupos");
        return grupoService.listarGrupo(grupo);
	}
	
}
