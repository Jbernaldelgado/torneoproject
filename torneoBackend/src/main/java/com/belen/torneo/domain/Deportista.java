package com.belen.torneo.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;

@Entity
@Table(schema = "public", name = "bt_deportista_tb")
public class Deportista implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1603854409866593403L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "identificacion")
	private String identificacion;
	
	@Column(name = "tipo_identificacion")
	private Integer tipoIdentificacion;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "apellido")
	private String apellido;

	@Column(name = "fecha_nacimiento")
	private LocalDate fechaNacimiento;
	
	@Column(name = "carnet")
	private String carnet;	
    
	@Column(name = "foto_deportista")
	private String fotoDeportista;
    
	@Column(name = "numero_camiseta")
	private Integer numeroCamiseta;
		
	@ManyToOne(optional = false)	
    @NotNull
    @JsonIgnoreProperties("deportistas")
	private Equipo idEquipo;
	
	@Column(name = "posicion")
	private String posicion;
    
	@Column(name = "goles_recibidos")
	private Integer golesRecibidos;
    
	@Column(name = "anotaciones")
	private Integer anotaciones;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public Integer getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(Integer tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCarnet() {
		return carnet;
	}

	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}

	public String getFotoDeportista() {
		return fotoDeportista;
	}

	public void setFotoDeportista(String fotoDeportista) {
		this.fotoDeportista = fotoDeportista;
	}

	public Integer getNumeroCamiseta() {
		return numeroCamiseta;
	}

	public void setNumeroCamiseta(Integer numeroCamiseta) {
		this.numeroCamiseta = numeroCamiseta;
	}

	public Equipo getIdEquipo() {
		return idEquipo;
	}

	public void setIdEquipo(Equipo idEquipo) {
		this.idEquipo = idEquipo;
	}

	public String getPosicion() {
		return posicion;
	}

	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}

	public Integer getGolesRecibidos() {
		return golesRecibidos;
	}

	public void setGolesRecibidos(Integer golesRecibidos) {
		this.golesRecibidos = golesRecibidos;
	}

	public Integer getAnotaciones() {
		return anotaciones;
	}

	public void setAnotaciones(Integer anotaciones) {
		this.anotaciones = anotaciones;
	}

}
